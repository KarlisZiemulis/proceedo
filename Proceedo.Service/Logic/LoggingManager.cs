﻿using System;
using System.Diagnostics;
using System.Text;

namespace Proceedo.Service.Logic
{
    public static class LoggingManager
    {
        public static readonly string Source = "ProceedoTile";
        public static readonly string Log = "Application";
        public static void LogError(Exception ex, string additionalComment = "")
        {
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(additionalComment))
            {
                sb.Append(additionalComment);
                sb.Append(Environment.NewLine);
            }
            sb.Append(ex.Message);
            sb.Append(Environment.NewLine);
            sb.Append(ex.StackTrace);

            //if (!EventLog.SourceExists(Source))
            //    EventLog.CreateEventSource(Source, Log); 
            //EventLog.WriteEntry(Source, sb.ToString(), EventLogEntryType.Error);
        }
    }
}
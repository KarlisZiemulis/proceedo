﻿using Proceedo.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Proceedo.Service.Logic
{
    public static class ProceedoResponseManager
    {
        public static void GetProceedoResponseViewModel(ref ProceedoResponseViewModel proceedoModel, JsonResult response, string nameId)
        {
            proceedoModel.proceedoUrl = string.Format(ConfigurationManager.AppSettings.Get("ProceedoInvoicesURL"), nameId);
            try
            {

                JavaScriptSerializer jss = new JavaScriptSerializer();

                Dictionary<string, string> obj = jss.Deserialize<Dictionary<string, string>>(response.Data.ToString());
                
                FillProceedoResponseViewModel(obj, proceedoModel, nameId);
                //return proceedoModel;

            }
            catch(Exception ex)
            {
                LoggingManager.LogError(ex);
                proceedoModel.ErrorMessage = ex.Message;
                //return proceedoModel;
            }
        }

        private static void FillProceedoResponseViewModel(Dictionary<string, string> obj, ProceedoResponseViewModel proceedoModel, string nameId)
        {
            int value = 0;
            int sum = 0;
            foreach (PropertyInfo property in typeof(ProceedoResponseViewModel).GetProperties().Where(p => Attribute.IsDefined(p, typeof(RequestType))))
            {
                if (obj.Keys.Contains(property.Name))
                {
                    int.TryParse(obj[property.Name], out value);
                }
                else value = 0;
                sum += value;
                property.SetValue(proceedoModel, value);
            }
            proceedoModel.invoiceSum = sum;

            proceedoModel.proceedoUrl = string.Format(ConfigurationManager.AppSettings.Get("ProceedoURL"), nameId);

        }
    }
}
﻿using System;
using System.Configuration;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;


namespace Proceedo.Service.Logic
{
    public static class JwtManager
    {
        public static string GenerateToken(string username, string issuer, string audience, int expireMinutes = 5)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var signingCert = GetPrivateKey();
                var now = DateTime.UtcNow;
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new ClaimsIdentity(new[]
                    {
                    new Claim("sub", username)
                })),
                    AppliesToAddress = ConfigurationManager.AppSettings.Get("Audience"),
                    TokenIssuerName = issuer,
                    SigningCredentials = new X509SigningCredentials(signingCert),
                    Lifetime = new Lifetime(now, now.AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings.Get("ExpirationTimeMin"))).ToUniversalTime())
                };

                var stoken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(stoken);

                return token;
            }
            catch (Exception ex)
            {
                LoggingManager.LogError(ex);
                return "Error " + ex.Message;
            }
        }
        private static JwtSecurityToken ReadToken(string encodedToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.ReadToken(encodedToken) as JwtSecurityToken;
            
            return token;
        }
       
        private static X509Certificate2 GetPrivateKey()
        {
            try
            {
                X509Store certStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                certStore.Open(OpenFlags.ReadOnly);
                X509Certificate2Collection collection = certStore.Certificates.Find(X509FindType.FindByThumbprint, ConfigurationManager.AppSettings
                    .Get("ProceedoTokenSigningCertThumbprint").Replace("\u200e", string.Empty).Replace("\u200f", string.Empty), false);
                certStore.Close();
                if (collection.Count == 0)
                    throw new CryptographicException("No certficate found");
                foreach (X509Certificate2 cert in collection)
                {
                    return cert;
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return null;
        }
    }
}
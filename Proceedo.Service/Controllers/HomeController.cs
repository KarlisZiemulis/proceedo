﻿using Aditro.Foundation.Logon.Utilities;
using Proceedo.Service.ViewModels;
using System.Security.Claims;
using System.Threading;
using System.Web.Mvc;

namespace Proceedo.Service.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index(string errorMessage = "")
        {

            var email = ClaimHelper.ReadValueFromClaim((ClaimsIdentity)Thread.CurrentPrincipal.Identity, ClaimTypes.Email);

            if (email != null)
            {
                return RedirectToAction("LoadData", "Proceedo");
            }
            var model = new AccountViewModel();
            model.Username = email;
            model.ErrorMessage = errorMessage;
            model.Issuer = "Test1349VM";
            model.Audience = "http://tohandle.proceedo.net";
            model.Expiration = 1;
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(AccountViewModel model)
        {
            return RedirectToAction("GetInvoices", "Proceedo", new { username = model.Username, issuer = model.Issuer, audience = model.Audience, exp = model.Expiration, test = true });
        }
    }
}
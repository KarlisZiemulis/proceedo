﻿using Aditro.Foundation.Logon.Utilities;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;

namespace Proceedo.Service.Controllers
{
    public abstract class MainController : Controller
    {
        private void SetLanguage(string language)
        {
            if (string.IsNullOrEmpty(language))
                language = "en-US";
            var culture = CultureInfo.CreateSpecificCulture(language);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            SetLanguage(ClaimHelper.GetUserLanguage());

            base.OnActionExecuting(filterContext);
        }

       

    }

}
﻿using Aditro.Foundation.Logon.Utilities;
using Proceedo.Service.Logic;
using Proceedo.Service.ViewModels;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Proceedo.Service.Controllers
{

    public class ProceedoController : MainController
    {
        private const string Tenant = "http://schemas.devexternal.com/identity/claims/2011/11/tenantid";
        private bool isError = false;
        // GET: Proceedo
        [Authorize]
        public ActionResult LoadData()
        {
            isError = false;
            var email = ClaimHelper.ReadValueFromClaim((ClaimsIdentity)Thread.CurrentPrincipal.Identity, ClaimTypes.Email);
            var issuer = ClaimHelper.ReadValueFromClaim((ClaimsIdentity)Thread.CurrentPrincipal.Identity, Tenant);
            var nameId = ClaimHelper.ReadValueFromClaim((ClaimsIdentity)Thread.CurrentPrincipal.Identity, ClaimTypes.NameIdentifier);

            int.TryParse(ConfigurationManager.AppSettings.Get("ExpirationTimeMin"), out int expiration);
            

            var response = GetInvoices(email, "Test1349VM", ConfigurationManager.AppSettings.Get("Audience"), expiration);

            var proceedoModel = new ProceedoResponseViewModel();
            if (!isError)
                ProceedoResponseManager.GetProceedoResponseViewModel(ref proceedoModel, response, nameId);
            return View(proceedoModel);
        }

       

        [Authorize]
        private JsonResult GetInvoices(string username, string issuer, string audience, int exp)
        {
            var url = ConfigurationManager.AppSettings.Get("ProceedoRequestURL");
            var authoriztionToken = "Bearer " + JwtManager.GenerateToken(username, issuer, audience, exp);
            if(authoriztionToken.StartsWith("Bearer Error"))
                return Json(authoriztionToken, JsonRequestBehavior.AllowGet);

            try
            {
                //REQUISITIONS,INVOICES,INVOICES_IMMINENT_DUE_DATE,INVALID_REQUISITIONS_RULE,GOOD_RECEIVAL,SAVED_REQUISITIONS,SYSTEM_MESSAGES,IMPORTS,DUE_COMPETITIONS,SHOPPING_BASKETS,BUDGETS
                url += "type=" + ConfigurationManager.AppSettings.Get("Type");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.Headers.Add("Authorization", authoriztionToken);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream resStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(resStream);
                string responseFromServer = reader.ReadToEnd();
                return Json(responseFromServer, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string additionalComment = "call from " + url + " returned:";
                LoggingManager.LogError(ex, additionalComment);
                isError = true;
                var error = new { error = ex.Message };
                return Json(new JavaScriptSerializer().Serialize(error), JsonRequestBehavior.AllowGet);
            }

        }

    }
}
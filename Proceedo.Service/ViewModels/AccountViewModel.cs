﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Proceedo.Service.ViewModels
{
    public class AccountViewModel
    {
        [DisplayName("User name")]
        public string Username { get; set; }

        [DisplayName("Issuer")]
        public string Issuer { get; set; }

        [DisplayName("Audience")]
        public string Audience { get; set; }

        [DisplayName("Password")]
        public string Password { get; set; }

        [DisplayName("Expiration")]
        public int Expiration { get; set; }

        [DisplayName("Test")]
        public int test { get; set; }

        [DisplayName("Error message")]
        public string ErrorMessage { get; set; }
    }
}
﻿using Proceedo.Service.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace Proceedo.Service.ViewModels
{
    public class ProceedoResponseViewModel
    {
        [RequestType]
        public int requisitionApprovals { get; set; } = 0;
        
        [RequestType]
        public int invoicesToHandle { get; set; } = 0;

        [RequestType]
        public int invoicesImminentDue { get; set; } = 0;

        [RequestType]
        public int nrOfRequisitionsWithInvalidRule { get; set; } = 0;

        [RequestType]
        public int goodsReceivals { get; set; } = 0;

        [RequestType]
        public int savedRequistions { get; set; } = 0;

        [RequestType]
        public int systemMessagesToApprove { get; set; } = 0;

        [RequestType]
        public int importsToHandle { get; set; } = 0;

        [RequestType]
        public int dueCompetitions { get; set; } = 0;

        [RequestType]
        public int shoppingBaskets { get; set; } = 0;

        [RequestType]
        public int nrOfBudgetsToApprove { get; set; } = 0;

        public string ErrorMessage { get; set; }
        public int invoiceSum { get; set; } = 0;
        public string proceedoUrl { get; set; }
    }
    public class RequestType : Attribute
    {
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proceedo.Service.Proceedo
{
    public static class RequestTypeStat
    {
        public static readonly string requisitionApprovals = "REQUISITIONS";
        public static readonly string invoicesToHandle = "INVOICES";
        public static readonly string invoicesImminentDue = "INVOICES_IMMINENT_DUE_DATE";
        public static readonly string nrOfRequisitionsWithInvalidRule = "INVALID_REQUISITIONS_RULE";
        public static readonly string goodsReceivals = "GOOD_RECEIVAL";
        public static readonly string savedRequistions = "SAVED_REQUISITIONS";
        public static readonly string systemMessagesToApprove = "SYSTEM_MESSAGES";
        public static readonly string importsToHandle = "IMPORTS";
        public static readonly string dueCompetitions = "DUE_COMPETITIONS";
        public static readonly string shoppingBaskets = "SHOPPING_BASKETS";
        public static readonly string nrOfBudgetsToApprove = "BUDGETS";
    }
}